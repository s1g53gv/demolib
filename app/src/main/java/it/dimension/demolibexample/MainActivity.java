package it.dimension.demolibexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import it.dimension.demolib.DemoUtilities;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DemoUtilities.doSomething();
    }
}
